# Git Internals

A project to demonstrate git internals

## Setting up

1. Initiate an empty git repository

```
git init gitinternals

```

2. Create 2 files

```
echo '# Git Internals

A project to demonstrate the inner workings of git' > README.md

echo 'package main

import "fmt"

func main() {
    fmt.Println("Git will take care of me.")
}
' > main.go
```

```
git add README.md main.go
```

```
git commit -m "Initial commit"
```
